package br.com.dals.barberapp.utils;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "barber-app")
public class ApplicationProperties {

    private Integer resourceServiceMaxCharacter;
}
