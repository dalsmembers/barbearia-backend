package br.com.dals.barberapp.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class CryptUtil {

    private static final int ROUNDS = 12;

    @Value(value = "${auth.secret}")
    private String SECRET;

    public String makeToken(String email, String password){
        String date = LocalDate.now().toString() + email + password + "."+SECRET;
        return BCrypt.hashpw(date, BCrypt.gensalt(ROUNDS));
    }

    public String makePasswordHash(String password){
        String passwordChanged = password;
        return BCrypt.hashpw(passwordChanged,BCrypt.gensalt());
    }

    public boolean validatePassword(String password, String hashPw){
        return BCrypt.checkpw(password,hashPw);
    }
}
