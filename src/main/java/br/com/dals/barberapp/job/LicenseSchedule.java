package br.com.dals.barberapp.job;

import br.com.dals.barberapp.entity.CompanyEntity;
import br.com.dals.barberapp.repository.CompanyRepository;
import br.com.dals.barberapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class LicenseSchedule {

    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;

    @Scheduled(cron = "0 */5 * ? * *")
    private void activeLicense(){
        try {
            active();
            inactive();
        }catch (Exception e){
            log.error("C=LicenseSchedule M=activeLicense ERROR={}", e.getMessage());
        }
    }

    @Transactional
    private void active() {
        List<CompanyEntity> companiesInactive = companyRepository.findByActive(Boolean.FALSE);
        for(CompanyEntity companyEntity : companiesInactive){
            if(companyEntity.getLicenseActive() && (null == companyEntity.getUpdate() || companyEntity.getUpdate().isBefore(LocalDate.now()))){
                companyEntity.setActive(Boolean.TRUE);
                companyEntity.setUpdate(LocalDate.now());
                companyEntity.setLicenseActive(Boolean.FALSE);
                companyRepository.save(companyEntity);
                companyEntity.getUserEntities().forEach(user -> {
                    user.setActive(Boolean.TRUE);
                    user.setUpdate(LocalDate.now());
                    userRepository.save(user);
                });
            }
        };
    }

    @Transactional
    private void inactive() {
        List<CompanyEntity> companies = companyRepository.findAllOneMonthAgo();
        for(CompanyEntity companyEntity : companies){
            companyEntity.setActive(Boolean.FALSE);
            companyEntity.setUpdate(LocalDate.now());
            companyRepository.save(companyEntity);
            companyEntity.getUserEntities().forEach(user -> {
                user.setActive(Boolean.FALSE);
                user.setUpdate(LocalDate.now());
                userRepository.save(user);
            });
        };
    }
}
