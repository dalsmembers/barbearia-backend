package br.com.dals.barberapp.job;

import br.com.dals.barberapp.repository.AccessTokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CheckTokenSchedule {

    private final AccessTokenRepository accessTokenRepository;

    @Scheduled(cron = "0 0 * ? * *")
    public void verifyTokenTime(){
        accessTokenRepository.invalidToken();
    }
}
