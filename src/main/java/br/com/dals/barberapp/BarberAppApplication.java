package br.com.dals.barberapp;

import br.com.dals.barberapp.utils.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableScheduling
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableConfigurationProperties(ApplicationProperties.class)
@EntityScan(basePackages = "br.com.dals.barberapp.entity")
@SpringBootApplication(scanBasePackages = "br.com.dals.barberapp")
@EnableJpaRepositories(basePackages = "br.com.dals.barberapp.repository")
public class BarberAppApplication {

	public static void main(String... args) {
		SpringApplication.run(BarberAppApplication.class, args);
	}

}
