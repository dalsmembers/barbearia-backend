package br.com.dals.barberapp.interceptor;

import br.com.dals.barberapp.annotation.TokenNoValidate;
import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import br.com.dals.barberapp.exception.UserException;
import br.com.dals.barberapp.repository.AccessTokenRepository;
import br.com.dals.barberapp.utils.Constant;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import springfox.documentation.swagger.web.ApiResourceController;
import springfox.documentation.swagger2.web.Swagger2Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

@Component
@RequiredArgsConstructor
public class RequestInterceptor extends HandlerInterceptorAdapter {

    private final AccessTokenRepository accessTokenRepository;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (handler instanceof HandlerMethod) {

            HandlerMethod hm = (HandlerMethod) handler;
            Method method = hm.getMethod();

            if (hm.getMethod().getDeclaringClass().getName().equals(ApiResourceController.class.getName())
                    || hm.getMethod().getDeclaringClass().getName().equals(Swagger2Controller.class.getName())
                    || hm.getMethod().getDeclaringClass().getName().equals(BasicErrorController.class.getName())) {
                return true;
            }

            if (!method.isAnnotationPresent(TokenNoValidate.class)) {
                validateToken(request);
            }
        }

        return true;
    }

    private Boolean validateToken(HttpServletRequest request) throws UserException {
        String token = request.getHeader(Constant.ACCESS_TOKEN);

        if(null != token){
            Boolean isValid = accessTokenRepository.verifyToken(token);

            if(isValid){
                return isValid;
            }
        }

        throw new UserException(MessageDefinitionEnum._9998);
    }
}
