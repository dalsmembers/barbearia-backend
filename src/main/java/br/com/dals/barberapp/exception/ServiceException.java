package br.com.dals.barberapp.exception;

import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceException extends Exception {

    private MessageDefinitionEnum messageDefinitionEnum;

    private String value;

    public ServiceException(MessageDefinitionEnum messageDefinitionEnum){
        this.messageDefinitionEnum = messageDefinitionEnum;
    }

    public ServiceException(MessageDefinitionEnum messageDefinitionEnum, String value){
        this.messageDefinitionEnum = messageDefinitionEnum;
        this.value = value;
    }
}
