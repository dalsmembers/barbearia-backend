package br.com.dals.barberapp.exception;

import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class CompanyException extends Exception {

    private MessageDefinitionEnum messageDefinitionEnum;

    public CompanyException(MessageDefinitionEnum messageDefinitionEnum){
        this.messageDefinitionEnum = messageDefinitionEnum;
    }
}
