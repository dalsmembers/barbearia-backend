package br.com.dals.barberapp.enums;

import org.springframework.security.core.GrantedAuthority;

public enum UserTypeEnum implements GrantedAuthority {

    ADMIN("Administrador"),
    BARBER("Barbeiro");

    private String description;

    UserTypeEnum(String description) {
        this.description = description;
    }

    public static UserTypeEnum getTypeByName(String userType) {
        for(UserTypeEnum e : UserTypeEnum.values()){
            if(e.name() .equals(userType)){
                return e;
            }
        }
        return UserTypeEnum.BARBER;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getAuthority() {
        return this.name();
    }
}
