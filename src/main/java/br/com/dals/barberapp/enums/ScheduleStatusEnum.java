package br.com.dals.barberapp.enums;

public enum ScheduleStatusEnum {

    SCHEDULED("Agendado"),
    CANCELED("Cancelado"),
    CONCLUDED("Concluído");

    ScheduleStatusEnum(String description) {
        this.description = description;
    }

    private String description;

    public String getDescription() {
        return description;
    }
}
