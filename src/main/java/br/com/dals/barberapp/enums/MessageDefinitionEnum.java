package br.com.dals.barberapp.enums;

import org.springframework.http.HttpStatus;

public enum MessageDefinitionEnum {

    _9999("9999","Serviço temporáriamente indisponível!.", HttpStatus.INTERNAL_SERVER_ERROR),
    _9998("9998","Token inválido!.", HttpStatus.BAD_REQUEST),
    _9997("9997","Usuário não pode realizar esta ação. Verifique a empresa informado no payload.", HttpStatus.BAD_REQUEST),
    _9996("9996","Recurso com id %s não foi encontrado.", HttpStatus.NOT_FOUND),


    _0001("0001","Password inválido.", HttpStatus.BAD_REQUEST),
    _0002("0002","Usuário não foi encontrado.", HttpStatus.NOT_FOUND),
    _0003("0003","Email não foi informado.", HttpStatus.BAD_REQUEST),
    _0004("0004","Email já cadastrado.", HttpStatus.BAD_REQUEST),
    _0005("0005","Usuário cadastrado com sucesso.", HttpStatus.OK),
    _0006("0006","Cadastro empresárial desabilitado.", HttpStatus.FORBIDDEN),
    _0007("0007","Usuário desabilitado.", HttpStatus.FORBIDDEN),
    _0008("0008","Confirmação de password está diferente do password.", HttpStatus.BAD_REQUEST),
    _0009("0009","Confirmação de password inválido", HttpStatus.BAD_REQUEST),
    _0010("0010","Empresa deve ser informada para realizar o cadastro de usuário.", HttpStatus.BAD_REQUEST),
    _0011("0011","Nome da empresa deve ser informado.", HttpStatus.BAD_REQUEST),
    _0012("0012","CNPJ da empresa deve ser informado.", HttpStatus.BAD_REQUEST),
    _0013("0013","Já existe uma empresa com os dados informados.", HttpStatus.BAD_REQUEST),
    _0014("0014","Nome do serviço deve ser informado.", HttpStatus.BAD_REQUEST),
    _0015("0015","Nome do serviço deve ter no máximo %s caracteres.", HttpStatus.BAD_REQUEST),
    _0016("0016","O Preço do serviço deve ser informado.", HttpStatus.BAD_REQUEST),
    _0017("0017","Tempo mínimo para serviço é de 15 min.", HttpStatus.BAD_REQUEST),
    _0018("0018","Valor NULO não é aceito.", HttpStatus.BAD_REQUEST),

    _20000("20000","Usuário cadastrado com sucesso.", HttpStatus.CREATED),
    _20001("20001","Service removido com sucesso.", HttpStatus.ACCEPTED),

    ;
    private String code;

    private String message;

    private HttpStatus status;

    MessageDefinitionEnum(String code, String message, HttpStatus status) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
