package br.com.dals.barberapp.service;

import br.com.dals.barberapp.entity.CompanyEntity;
import br.com.dals.barberapp.entity.dto.request.CompanyDtoRequest;
import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import br.com.dals.barberapp.exception.CompanyException;
import br.com.dals.barberapp.repository.CompanyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class CompanyService {

    private final CompanyRepository companyRepository;

    @Transactional
    public CompanyEntity createCompany(CompanyDtoRequest company) throws CompanyException {
        isValidCompanyRequest(company);
        CompanyEntity entity = convertDtoToSaveEntity(company);
        return companyRepository.save(entity);
    }

    private CompanyEntity convertDtoToSaveEntity(CompanyDtoRequest company) {
        CompanyEntity entity = new CompanyEntity();
        entity.setActive(Boolean.FALSE);
        entity.setName(company.getName());
        entity.setCnpj(company.getCnpj());
        entity.setCreated(LocalDate.now());
        return entity;
    }

    private void isValidCompanyRequest(CompanyDtoRequest company) throws CompanyException {
        if(null == company){
            throw new CompanyException(MessageDefinitionEnum._0010);
        }
        if(StringUtils.isEmpty(company.getName())){
            throw new CompanyException(MessageDefinitionEnum._0011);
        }
        if(StringUtils.isEmpty(company.getCnpj())){
            throw new CompanyException(MessageDefinitionEnum._0012);
        }
        if(null != companyRepository.findByNameIgnoreCaseOrCnpj(company.getName(),company.getCnpj())){
            throw new CompanyException(MessageDefinitionEnum._0013);
        }
    }
}
