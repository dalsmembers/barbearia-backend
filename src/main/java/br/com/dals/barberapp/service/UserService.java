package br.com.dals.barberapp.service;

import br.com.dals.barberapp.entity.CompanyEntity;
import br.com.dals.barberapp.entity.UserEntity;
import br.com.dals.barberapp.entity.dto.request.UserDtoRequest;
import br.com.dals.barberapp.entity.dto.response.DefaultResponse;
import br.com.dals.barberapp.entity.dto.response.EnumDefaultDtoResponse;
import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import br.com.dals.barberapp.enums.UserTypeEnum;
import br.com.dals.barberapp.exception.CompanyException;
import br.com.dals.barberapp.exception.UserException;
import br.com.dals.barberapp.repository.UserRepository;
import br.com.dals.barberapp.utils.CryptUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final CompanyService companyService;

    private final CryptUtil cryptUtil;

    public List<EnumDefaultDtoResponse> findUserType() {
        List<EnumDefaultDtoResponse> userType = new ArrayList<>();
        for (UserTypeEnum value : UserTypeEnum.values()) {
            userType.add(new EnumDefaultDtoResponse(value.name(),value.getDescription()));
        }
        return userType;
    }

    @Transactional
    public DefaultResponse requestNewContact(UserDtoRequest user) throws UserException, CompanyException {
        isValidUserRequest(user);
        saveUserAdmin(user);
        return new DefaultResponse(MessageDefinitionEnum._20000);
    }

    private void saveUserAdmin(UserDtoRequest user) throws CompanyException {
        UserEntity userEntity = convertDtoToSaveEntity(user);
        CompanyEntity companyEntity = companyService.createCompany(user.getCompany());
        userEntity.setCompany(companyEntity);
        userRepository.save(userEntity);
    }

    private UserEntity convertDtoToSaveEntity(UserDtoRequest user) {
        UserEntity entity = new UserEntity();
        entity.setActive(Boolean.FALSE);
        entity.setEmail(user.getEmail());
        entity.setName(user.getName());
        entity.setUserType(UserTypeEnum.ADMIN);
        entity.setPassword(cryptUtil.makePasswordHash(user.getPassword()));
        entity.setCreated(LocalDate.now());
        return entity;
    }

    private void isValidUserRequest(UserDtoRequest user) throws UserException {
        if(StringUtils.isEmpty(user.getEmail())){
            throw new UserException(MessageDefinitionEnum._0003);
        }
        if(StringUtils.isEmpty(user.getPassword())){
            throw new UserException(MessageDefinitionEnum._0001);
        }
        if(StringUtils.isEmpty(user.getRepassword())){
            throw new UserException(MessageDefinitionEnum._0009);
        }
        if(!user.getPassword().equals(user.getRepassword())){
            throw new UserException(MessageDefinitionEnum._0008);
        }
        if(userRepository.findById(user.getEmail()).isPresent()){
            throw new UserException(MessageDefinitionEnum._0004);
        }
    }
}
