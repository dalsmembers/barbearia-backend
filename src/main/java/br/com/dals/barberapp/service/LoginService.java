package br.com.dals.barberapp.service;

import br.com.dals.barberapp.entity.AccessTokenEntity;
import br.com.dals.barberapp.entity.UserEntity;
import br.com.dals.barberapp.entity.dto.request.UserLoginDtoRequest;
import br.com.dals.barberapp.entity.dto.response.CompanyDtoResponse;
import br.com.dals.barberapp.entity.dto.response.UserDtoResponse;
import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import br.com.dals.barberapp.exception.UserException;
import br.com.dals.barberapp.repository.AccessTokenRepository;
import br.com.dals.barberapp.repository.UserRepository;
import br.com.dals.barberapp.utils.CryptUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final AccessTokenRepository accessTokenRepository;

    private final UserRepository userRepository;

    private final CryptUtil cryptUtil;

    @Transactional
    public UserDtoResponse login(UserLoginDtoRequest user) throws UserException {
        isValid(user);
        Optional<UserEntity> userOptional = userRepository.findById(user.getEmail());
        if(!userOptional.isPresent()){
            throw new UserException(MessageDefinitionEnum._0002);
        }
        UserEntity userEntityDB = userOptional.get();
        isAccessValid(userEntityDB);
        if(cryptUtil.validatePassword(user.getPassword(), userEntityDB.getPassword())){
            String token = cryptUtil.makeToken(userEntityDB.getEmail(), userEntityDB.getPassword());
            saveToken(userEntityDB.getEmail(), token);
            return createUserResponse(token, userEntityDB);
        }
        throw new UserException(MessageDefinitionEnum._0001);
    }

    private UserDtoResponse createUserResponse(String token, UserEntity userEntityDB) {
        UserDtoResponse response = new UserDtoResponse();
        response.setName(userEntityDB.getName());
        response.setAuthToken(token);
        response.setEmail(userEntityDB.getEmail());
        response.setCompany(new CompanyDtoResponse(userEntityDB.getCompany().getId(), userEntityDB.getCompany().getName()));
        return response;
    }

    private void isAccessValid(UserEntity userEntityDB) throws UserException {
        if(!userEntityDB.getCompany().getActive()){
            throw new UserException(MessageDefinitionEnum._0006);
        }
        if(!userEntityDB.getActive()){
            throw new UserException(MessageDefinitionEnum._0007);
        }
    }

    private void isValid(UserLoginDtoRequest user) throws UserException {
        if(StringUtils.isEmpty(user.getEmail())){
            throw new UserException(MessageDefinitionEnum._0003);
        }
        if(StringUtils.isEmpty(user.getPassword())){
            throw new UserException(MessageDefinitionEnum._0001);
        }
    }

    private void saveToken(String email, String token) {
        AccessTokenEntity accessTokenEntity;
        Optional<AccessTokenEntity> accessTokenOptional = accessTokenRepository.findById(email);
        if(accessTokenOptional.isPresent()){
            accessTokenEntity = accessTokenOptional.get();
            accessTokenEntity.setToken(token);
            accessTokenEntity.setUpdate(LocalDate.now());
            accessTokenEntity.setActive(Boolean.TRUE);
        }else{
            accessTokenEntity = new AccessTokenEntity();
            accessTokenEntity.setUser(email);
            accessTokenEntity.setCreated(LocalDate.now());
            accessTokenEntity.setActive(Boolean.TRUE);
            accessTokenEntity.setToken(token);
        }
        accessTokenRepository.save(accessTokenEntity);
    }
}
