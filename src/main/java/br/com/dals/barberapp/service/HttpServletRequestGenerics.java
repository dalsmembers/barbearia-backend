package br.com.dals.barberapp.service;

import br.com.dals.barberapp.entity.AccessTokenEntity;
import br.com.dals.barberapp.entity.UserEntity;
import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import br.com.dals.barberapp.exception.ServiceException;
import br.com.dals.barberapp.exception.UserException;
import br.com.dals.barberapp.repository.AccessTokenRepository;
import br.com.dals.barberapp.repository.UserRepository;
import br.com.dals.barberapp.utils.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Component
public abstract class HttpServletRequestGenerics {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private UserRepository userRepository;

    protected UserEntity getUser(HttpServletRequest request) throws UserException {
        try{
            String token = request.getHeader(Constant.ACCESS_TOKEN);
            if(null == token){
                throw new UserException(MessageDefinitionEnum._9998);
            }
            Boolean isValid = accessTokenRepository.verifyToken(token);
            List<AccessTokenEntity> tokens = accessTokenRepository.findByToken(token);
            if(tokens.size() == 1 && tokens.get(0).getActive()){
                return userRepository.findById(tokens.get(0).getUser()).get();
            }
        }catch (Exception e){
            log.error("C=HttpServletRequestGenerics M=getUser error={}", e.getMessage());
            throw new UserException(MessageDefinitionEnum._9999);
        }
        throw new UserException(MessageDefinitionEnum._0002);
    }

    protected void isValidCompanyThisUser(Long companyId, HttpServletRequest request) throws ServiceException, UserException {
        String token = request.getHeader(Constant.ACCESS_TOKEN);
        if(null != token){
            UserEntity userLogged = userRepository.findByTokenIsActive(token);
            if(null != userLogged ){
                throw new UserException(MessageDefinitionEnum._0002);
            }
            if(companyId != userLogged.getCompany().getId()){
                throw new ServiceException(MessageDefinitionEnum._9997);
            }
        }
    }
}
