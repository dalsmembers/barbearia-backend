package br.com.dals.barberapp.service;

import br.com.dals.barberapp.entity.ServiceEntity;
import br.com.dals.barberapp.entity.UserEntity;
import br.com.dals.barberapp.entity.dto.request.ServiceDtoRequest;
import br.com.dals.barberapp.entity.dto.response.DefaultResponse;
import br.com.dals.barberapp.entity.dto.response.ServiceDtoResponse;
import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import br.com.dals.barberapp.exception.ServiceException;
import br.com.dals.barberapp.exception.UserException;
import br.com.dals.barberapp.repository.ServiceRepository;
import br.com.dals.barberapp.utils.ApplicationProperties;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ServiceService extends HttpServletRequestGenerics {

    private final ServiceRepository serviceRepository;

    private final ApplicationProperties properties;

    public List<ServiceDtoResponse> findAll(HttpServletRequest request) throws UserException {
        UserEntity user = getUser(request);
        List<ServiceDtoResponse> response = new ArrayList<>();
        List<ServiceEntity> services = serviceRepository.findByCompanyId(user.getCompany().getId());
        services.forEach(entity -> {
            response.add(convertToDto(entity));
        });
        return response;
    }

    private ServiceDtoResponse convertToDto(ServiceEntity entity) {
        return new ServiceDtoResponse(
                entity.getId(),
                entity.getName(),
                entity.getAverageDuration(),
                entity.getPrice());
    }

    public ServiceDtoResponse create(ServiceDtoRequest dto, HttpServletRequest request) throws ServiceException, UserException {
        isValid(dto);
        isValidCompanyThisUser(dto.getCompanyId(), request);
        return convertToDto(serviceRepository.save(convertRequestToEntity(dto)));
    }

    private ServiceEntity convertRequestToEntity(ServiceDtoRequest request) {
        ServiceEntity entity = new ServiceEntity();
        entity.setName(request.getName());
        entity.setAverageDuration(request.getAverageDuration());
        entity.setPrice(request.getPrice());
        return entity;
    }

    private void isValid(ServiceDtoRequest dto) throws ServiceException {
        if(StringUtils.isEmpty(dto.getName())) {
            throw new ServiceException(MessageDefinitionEnum._0014);
        }
        if(properties.getResourceServiceMaxCharacter() < dto.getName().length()){
            throw new ServiceException(MessageDefinitionEnum._0015);
        }
        if(null == dto.getPrice() || dto.getPrice() == 0D){
            throw new ServiceException(MessageDefinitionEnum._0016);
        }
        if(null == dto.getAverageDuration() ||
                dto.getAverageDuration().isBefore(LocalTime.of(0,15))){
            throw new ServiceException(MessageDefinitionEnum._0017);
        }
    }

    public DefaultResponse delete(Long id, HttpServletRequest request) throws ServiceException, UserException {
        isValid(id);
        Optional<ServiceEntity> serviceEntity = serviceRepository.findById(id);
        if(!serviceEntity.isPresent()){
            throw new ServiceException(MessageDefinitionEnum._9996, id.toString());
        }
        isValidCompanyThisUser(serviceEntity.get().getCompany().getId(), request);
        serviceRepository.deleteById(id);
        return new DefaultResponse(MessageDefinitionEnum._20001);
    }

    private void isValid(Long id) throws ServiceException {
        if(null == id)
            throw new ServiceException(MessageDefinitionEnum._0018);
    }
}
