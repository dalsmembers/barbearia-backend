package br.com.dals.barberapp.controller;

import br.com.dals.barberapp.entity.dto.request.ServiceDtoRequest;
import br.com.dals.barberapp.entity.dto.response.DefaultResponse;
import br.com.dals.barberapp.entity.dto.response.ServiceDtoResponse;
import br.com.dals.barberapp.exception.ServiceException;
import br.com.dals.barberapp.exception.UserException;
import br.com.dals.barberapp.service.ServiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "service")
public class ServiceController {

    private final ServiceService serviceService;

    @GetMapping
    public ResponseEntity<List<ServiceDtoResponse>> findAll(HttpServletRequest request) throws UserException {
        return new ResponseEntity(serviceService.findAll(request), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ServiceDtoResponse> create(@RequestBody ServiceDtoRequest dto, HttpServletRequest request) throws ServiceException, UserException {
        return new ResponseEntity(serviceService.create(dto, request), HttpStatus.OK);
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<DefaultResponse> delete(@PathVariable Long id, HttpServletRequest request) throws ServiceException, UserException {
        return new ResponseEntity(serviceService.delete(id,request), HttpStatus.OK);
    }

}
