package br.com.dals.barberapp.controller;

import br.com.dals.barberapp.annotation.TokenNoValidate;
import br.com.dals.barberapp.entity.dto.request.UserLoginDtoRequest;
import br.com.dals.barberapp.entity.dto.response.UserDtoResponse;
import br.com.dals.barberapp.exception.UserException;
import br.com.dals.barberapp.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "auth")
public class LoginController {

    private final LoginService loginService;

    @TokenNoValidate
    @PostMapping
    public ResponseEntity<UserDtoResponse> login(@RequestBody UserLoginDtoRequest user) throws UserException {
        return new ResponseEntity(loginService.login(user), HttpStatus.OK);
    }
}
