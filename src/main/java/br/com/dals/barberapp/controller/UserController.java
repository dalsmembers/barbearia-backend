package br.com.dals.barberapp.controller;

import br.com.dals.barberapp.annotation.TokenNoValidate;
import br.com.dals.barberapp.entity.dto.request.UserDtoRequest;
import br.com.dals.barberapp.entity.dto.request.UserLoginDtoRequest;
import br.com.dals.barberapp.entity.dto.response.DefaultResponse;
import br.com.dals.barberapp.entity.dto.response.EnumDefaultDtoResponse;
import br.com.dals.barberapp.entity.dto.response.UserDtoResponse;
import br.com.dals.barberapp.exception.CompanyException;
import br.com.dals.barberapp.exception.UserException;
import br.com.dals.barberapp.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "user")
public class UserController {

    private final UserService userService;

    @TokenNoValidate
    @GetMapping(path = "/type")
    public ResponseEntity<List<EnumDefaultDtoResponse>> findUserType(){
        return new ResponseEntity<List<EnumDefaultDtoResponse>>(userService.findUserType(), HttpStatus.OK);
    }

    @PostMapping("/request")
    @ApiOperation(value = "Solicitação de cadastro de EMPRESA e USUÁRIO com perfil ADMIN.")
    public ResponseEntity<DefaultResponse> requestNewContact(@RequestBody UserDtoRequest user) throws UserException, CompanyException {
        return new ResponseEntity<DefaultResponse>(userService.requestNewContact(user), HttpStatus.OK);
    }
}
