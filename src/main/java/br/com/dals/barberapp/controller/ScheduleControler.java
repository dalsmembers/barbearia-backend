package br.com.dals.barberapp.controller;

import br.com.dals.barberapp.entity.dto.response.ServiceDtoResponse;
import br.com.dals.barberapp.service.ScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "schedule")
public class ScheduleControler {

    private final ScheduleService scheduleService;

    @GetMapping
    public ResponseEntity<ServiceDtoResponse> findAll(){
        return new ResponseEntity(scheduleService, HttpStatus.OK);
    }

}
