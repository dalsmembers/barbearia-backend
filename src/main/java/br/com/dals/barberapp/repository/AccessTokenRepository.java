package br.com.dals.barberapp.repository;

import br.com.dals.barberapp.entity.AccessTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccessTokenRepository extends JpaRepository<AccessTokenEntity, String> {

    @Query("SELECT t.active FROM AccessTokenEntity t WHERE t.token = ?1")
    Boolean verifyToken(String token);

    @Modifying
    @Query("UPDATE AccessTokenEntity a " +
            " SET a.active = false WHERE " +
            " (a.update = null and TIMEDIFF(now(), a.created ) > 1) OR " +
            " (a.update <> null and TIMEDIFF(now(), a.update ) > 1)")
    void invalidToken();

    List<AccessTokenEntity> findByToken(String token);
}
