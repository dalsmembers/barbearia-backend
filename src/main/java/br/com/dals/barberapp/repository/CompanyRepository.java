package br.com.dals.barberapp.repository;

import br.com.dals.barberapp.entity.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, Long> {

    CompanyEntity findByNameIgnoreCaseOrCnpj(String name, Long cnpj);

    List<CompanyEntity> findByActive(Boolean active);

    @Query(value = "select * from company where DATE_ADD(dat_updated, INTERVAL 1 MONTH) < CURRENT_TIMESTAMP()", nativeQuery = true)
    List<CompanyEntity> findAllOneMonthAgo();
}
