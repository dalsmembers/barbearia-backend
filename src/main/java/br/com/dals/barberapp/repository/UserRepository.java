package br.com.dals.barberapp.repository;

import br.com.dals.barberapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {

    @Query("Select DISTINCT u from UserEntity u, AccessTokenEntity t " +
            " WHERE t.user = u.email " +
            " AND t.token = ?1")
    UserEntity findByTokenIsActive(String token);

    UserEntity findByEmail(String username);
}
