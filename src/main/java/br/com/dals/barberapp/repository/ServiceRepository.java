package br.com.dals.barberapp.repository;

import br.com.dals.barberapp.entity.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<ServiceEntity, Long> {

    @Query("SELECT s FROM ServiceEntity s WHERE s.company.id = ?1")
    List<ServiceEntity> findByCompanyId(Long id);

}
