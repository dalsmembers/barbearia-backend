package br.com.dals.barberapp.advice;

import br.com.dals.barberapp.entity.dto.response.DefaultResponse;
import br.com.dals.barberapp.exception.CompanyException;
import br.com.dals.barberapp.exception.ServiceException;
import br.com.dals.barberapp.exception.UserException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ResponseExceptionAdvice {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<DefaultResponse> errorGeneric(Exception e){
        DefaultResponse response = new DefaultResponse();
        response.setCode(null);
        response.setMessage(null);
        response.setErrorMessage(e.getMessage());
        return new ResponseEntity<DefaultResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = UserException.class)
    public ResponseEntity<DefaultResponse> userException(UserException e){
        DefaultResponse response = new DefaultResponse();
        response.setCode(e.getMessageDefinitionEnum().getCode());
        response.setErrorMessage(e.getMessageDefinitionEnum().getMessage());
        return new ResponseEntity<DefaultResponse>(response, e.getMessageDefinitionEnum().getStatus());
    }

    @ExceptionHandler(value = CompanyException.class)
    public ResponseEntity<DefaultResponse> companyException(CompanyException e){
        DefaultResponse response = new DefaultResponse();
        response.setCode(e.getMessageDefinitionEnum().getCode());
        response.setErrorMessage(e.getMessageDefinitionEnum().getMessage());
        return new ResponseEntity<DefaultResponse>(response, e.getMessageDefinitionEnum().getStatus());
    }

    @ExceptionHandler(value = ServiceException.class)
    public ResponseEntity<DefaultResponse> serviceException(ServiceException e){
        DefaultResponse response = new DefaultResponse();
        response.setCode(e.getMessageDefinitionEnum().getCode());
        if(StringUtils.isEmpty(e.getValue())){
            response.setErrorMessage(e.getMessageDefinitionEnum().getMessage());
        }else{
            response.setErrorMessage(String.format(e.getMessageDefinitionEnum().getMessage(),e.getValue()));
        }
        return new ResponseEntity<DefaultResponse>(response, e.getMessageDefinitionEnum().getStatus());
    }
}
