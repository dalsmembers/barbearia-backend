package br.com.dals.barberapp.entity.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserLoginDtoRequest implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 8859303827274304832L;

    private String email;

    private String password;
}
