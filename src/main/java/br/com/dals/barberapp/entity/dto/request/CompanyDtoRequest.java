package br.com.dals.barberapp.entity.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyDtoRequest implements Serializable {

    private static final long serialVersionUID = 7153873105827538113L;

    private String name;
    private Long cnpj;
}
