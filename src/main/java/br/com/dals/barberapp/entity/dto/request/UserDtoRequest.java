package br.com.dals.barberapp.entity.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDtoRequest implements Serializable {

    private static final long serialVersionUID = 8590595518000810074L;

    private String email;
    private String name;
    private String password;
    private String repassword;
    private CompanyDtoRequest company;
}
