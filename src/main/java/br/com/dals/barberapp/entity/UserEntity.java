package br.com.dals.barberapp.entity;

import br.com.dals.barberapp.enums.UserTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;

@Entity
@Getter
@Setter
@Table(name = "user")
public class UserEntity implements UserDetails {

    @Id
    @Column(name = "des_email")
    private String email;

    @Column(name = "des_password")
    private String password;

    @Column(name = "des_name")
    private String name;

    @Column(name = "flg_active")
    private Boolean active;

    @Column(name = "dat_created")
    private LocalDate created;

    @Column(name = "dat_updated")
    private LocalDate update;

    @Column(name = "tp_user_type")
    @Enumerated(EnumType.STRING)
    private UserTypeEnum userType;

    @ManyToOne
    @JoinColumn(name = "idt_company", referencedColumnName = "idt_company")
    private CompanyEntity company;

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", created=" + created +
                ", update=" + update +
                ", userType=" + userType +
                ", company=" + company +
                '}';
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(userType);
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
