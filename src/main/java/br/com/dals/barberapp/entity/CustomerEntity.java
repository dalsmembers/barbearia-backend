package br.com.dals.barberapp.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "customer")
public class CustomerEntity implements Serializable {

    private static final long serialVersionUID = -3202950168174907024L;

    @Id
    @Column(name = "idt_customer")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "des_name")
    private String name;

    @Column(name = "dat_birthday")
    private String birthday;

    @Column(name = "dat_created")
    private String created;

    @Column(name = "dat_updated")
    private String update;

    @ManyToOne
    @JoinColumn(name = "idt_company", referencedColumnName = "idt_company")
    private CompanyEntity company;

}
