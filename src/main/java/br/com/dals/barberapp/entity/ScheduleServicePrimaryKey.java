package br.com.dals.barberapp.entity;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Getter
@Setter
@Embeddable
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
public class ScheduleServicePrimaryKey implements Serializable {

    private static final long serialVersionUID = 6637980345440846573L;
    @OneToOne
    @JoinColumn(name = "idt_schedule", referencedColumnName = "idt_schedule")
    private ScheduleEntity schedule;

    @OneToOne
    @JoinColumn(name = "idt_service", referencedColumnName = "idt_service")
    private ServiceEntity service;
}
