package br.com.dals.barberapp.entity;

import br.com.dals.barberapp.enums.ScheduleStatusEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "schedule")
public class ScheduleEntity implements Serializable {

    private static final long serialVersionUID = -7365733196939945905L;
    @Id
    @Column(name = "idt_schedule")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "des_status")
    @Enumerated(EnumType.STRING)
    private ScheduleStatusEnum status;

    @Column(name = "dat_schedule")
    private LocalDate schedule;

    @ManyToOne
    @JoinColumn(name = "idt_customer", referencedColumnName = "idt_customer")
    private CustomerEntity customer;
}
