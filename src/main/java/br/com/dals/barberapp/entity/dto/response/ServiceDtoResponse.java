package br.com.dals.barberapp.entity.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceDtoResponse implements Serializable {

    private static final long serialVersionUID = -7031898871539641307L;

    private Long id;
    private String name;
    private LocalTime averageDuration;
    private Double price;
}
