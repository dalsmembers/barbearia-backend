package br.com.dals.barberapp.entity.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalTime;

@Getter
@Setter
public class ServiceDtoRequest implements Serializable {
    private static final long serialVersionUID = -5122495782090306985L;

    private String name;
    private LocalTime averageDuration;
    private Double price;
    private Long companyId;
}
