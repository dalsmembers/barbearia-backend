package br.com.dals.barberapp.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "company")
public class CompanyEntity implements Serializable {

    private static final long serialVersionUID = -4679454262095457532L;

    @Id
    @Column(name = "idt_company")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "des_name")
    private String name;

    @Column(name = "des_cnpj")
    private Long cnpj;

    @Column(name = "flg_active")
    private Boolean active;

    @Column(name = "flg_license_active")
    private Boolean licenseActive;

    @Column(name = "dat_created")
    private LocalDate created;

    @Column(name = "dat_updated")
    private LocalDate update;

    @OneToMany(mappedBy = "company", fetch = FetchType.EAGER)
    private List<UserEntity> userEntities;

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cnpj=" + cnpj +
                ", active=" + active +
                ", licenseActive=" + licenseActive +
                ", created=" + created +
                ", update=" + update +
                '}';
    }
}
