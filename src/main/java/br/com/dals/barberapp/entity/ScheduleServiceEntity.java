package br.com.dals.barberapp.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "schedule_service")
public class ScheduleServiceEntity implements Serializable {

    private static final long serialVersionUID = 7152983351812709389L;

    @EmbeddedId
    private ScheduleServicePrimaryKey id;

    @Column(name = "num_value")
    private Double price;

}
