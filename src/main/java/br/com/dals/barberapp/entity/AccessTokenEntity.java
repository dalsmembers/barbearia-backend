package br.com.dals.barberapp.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "access_token")
public class AccessTokenEntity implements Serializable {

    private static final long serialVersionUID = 8042134663584916777L;

    @Id
    @Column(name = "des_email")
    private String user;

    @Column(name = "des_token")
    private String token;

    @Column(name = "flg_active")
    private Boolean active;

    @Column(name = "dat_created")
    private LocalDate created;

    @Column(name = "dat_updated")
    private LocalDate update;
}
