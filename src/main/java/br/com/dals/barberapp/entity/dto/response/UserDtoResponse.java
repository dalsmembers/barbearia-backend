package br.com.dals.barberapp.entity.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDtoResponse implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = -747701698662297858L;

    private String name;

    private String email;

    private String authToken;

    private CompanyDtoResponse company;
}
