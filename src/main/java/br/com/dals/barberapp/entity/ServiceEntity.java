package br.com.dals.barberapp.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalTime;

@Getter
@Setter
@Entity
@Table(name = "service")
public class ServiceEntity implements Serializable {

    private static final long serialVersionUID = 4169877151102037187L;
    @Id
    @Column(name = "idt_service")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "des_name")
    private String name;

    @Column(name = "tm_average_duration")
    private LocalTime averageDuration;

    @Column(name = "num_price")
    private Double price;

    @ManyToOne
    @JoinColumn(name = "idt_company", referencedColumnName = "idt_company")
    private CompanyEntity company;
}
