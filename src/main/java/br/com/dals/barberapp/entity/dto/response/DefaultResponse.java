package br.com.dals.barberapp.entity.dto.response;

import br.com.dals.barberapp.enums.MessageDefinitionEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DefaultResponse implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = -1220279238508593278L;

    private String code;

    private String message;

    private String errorMessage;

    public DefaultResponse(MessageDefinitionEnum message) {
        this.code = message.getCode();
        this.message = message.getMessage();
    }
}
