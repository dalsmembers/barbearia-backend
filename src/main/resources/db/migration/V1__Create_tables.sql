CREATE TABLE IF NOT EXISTS company (
    idt_company BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    des_name VARCHAR(250) NOT NULL,
    des_cnpj VARCHAR(15) NOT NULL,
    flg_active TINYINT DEFAULT 0 NOT NULL,
    dat_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dat_updated DATETIME NULL
)CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS user (
    des_email VARCHAR(50) NOT NULL PRIMARY KEY,
    idt_company BIGINT NOT NULL,
    des_password VARCHAR(250) NOT NULL,
    des_name VARCHAR(150) NOT NULL,
    flg_active TINYINT DEFAULT 0 NOT NULL,
    dat_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dat_updated DATETIME NULL,
    tp_user_type VARCHAR(50) NOT NULL,
    FOREIGN KEY (idt_company) REFERENCES company (idt_company)
)CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS access_token (
    des_email VARCHAR(50) NOT NULL PRIMARY KEY,
    des_token VARCHAR(250) NOT NULL,
    flg_active TINYINT DEFAULT 0 NOT NULL,
    dat_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dat_updated DATETIME NULL,
    FOREIGN KEY (des_email) REFERENCES user (des_email)
)CHARACTER SET utf8 COLLATE utf8_general_ci;