ALTER TABLE service ADD COLUMN idt_company BIGINT NOT NULL;
ALTER TABLE service ADD FOREIGN KEY (idt_company) REFERENCES company (idt_company);