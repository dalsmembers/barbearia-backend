CREATE TABLE IF NOT EXISTS customer (
    idt_customer BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    idt_company BIGINT NOT NULL,
    des_name VARCHAR(250) NOT NULL,
    dat_birthday DATE NOT NULL,
    dat_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dat_updated DATETIME NULL,
    FOREIGN KEY (idt_company) REFERENCES company (idt_company)
)CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS schedule (
    idt_schedule BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    idt_customer BIGINT NOT NULL,
    dat_schedule DATETIME NOT NULL,
    des_status VARCHAR(150) NOT NULL,
    FOREIGN KEY (idt_customer) REFERENCES customer (idt_customer)
)CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS service (
    idt_service BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    des_name VARCHAR(250) NOT NULL,
    tm_average_duration TIMESTAMP NOT NULL,
    num_price DOUBLE NOT NULL
)CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS schedule_service (
    idt_schedule BIGINT NOT NULL,
    idt_service BIGINT NOT NULL,
    num_value DOUBLE NOT NULL,
    PRIMARY KEY (idt_schedule, idt_service),
    FOREIGN KEY (idt_schedule) REFERENCES schedule (idt_schedule),
    FOREIGN KEY (idt_service) REFERENCES service (idt_service)
)CHARACTER SET utf8 COLLATE utf8_general_ci;