FROM openjdk:8-jdk-slim

LABEL maintainer 'Gabriel Morais Barbosa <gabrielmbdals@gmail.com>'

COPY ./build/libs/barber-app*.jar /opt/barberapp/app.jar

CMD ["java", "-jar", "/opt/barberapp/app.jar"]